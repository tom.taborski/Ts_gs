# Ts Gs

Project that publish the code necessary to calculate the different gs and produce graphs of the following paper: DOI

# Structure

-   I aim to put the data (raw and final) and other necessary info in data/
-   I would like to create a package folder with packrat
-   A folder in which i put all the script used to produce the data from raw and determine gs.

*I hope I changed all the strange variable name or comments from the script. Some may still be present :S*

# scripts/

Folder that stores all the .R files which are used to determine the stomatal conductance using the different methods. Normally each files can be executed on its own (line by line) and can also be called by others when outputs from them are required.

-   Energy_storage.R: compute the different storage elements used in the PM inversions
-   file_opening.R: simple file to open the data.
-   ga_determination.R: compute the different aerodynamic conductance required in the different gs methods.
-   gs_computation.R: compute the 4 different methods used to determined gs.
-   general_variables.R: contains different global variables used in figures (colour palettes, textsize, etc)
-   tentatives.R: should not exist in the final version it's a temporary script on which I test ways to create the figures without destroying everything.
-   figure\_\*.R: files that specifically produces the figures.

# graphes/

2020 environmental conditions

![test](graphes/figure_env_season2.jpeg)

Daily gs over the summer: this figures aims to present gs daily pattern and the difference among the methods (EC not shown here).

![](graphes/figure_daily_gs.jpeg)

Isothermal assumption and its effect on gs determination:

![](graphes/figure_Ts_to_gs.jpeg)

# data/

Contains the data used in the article to analyse and produce the graphs.

-   biomet.csv

    |              |                     |              |             |
    |:------------:|:-------------------:|:------------:|:-----------:|
    | **Variable** |      **Units**      | **Variable** |  **Units**  |
    |  TIMESTAMP   | YYYY-MM-DD HH:MM:SS |      Rg      |    W.m-2    |
    |     qc_P     |       logical       |      Pa      |     Pa      |
    |     LWin     |        W.m-2        |      P       |     mm      |
    |    LWout     |        W.m-2        |      H       |    W.m-2    |
    |     SWin     |        W.m-2        |      LE      |    W.m-2    |
    |    SWout     |        W.m-2        |    ustar     |    m.S-1    |
    |      Rn      |        W.m-2        |  GPP_DT_U50  | **à vérif** |

-   SWC.csv

    | **Variable** |      **Units**      | **Variable** |   **Units**   |
    |:------------:|:-------------------:|:------------:|:-------------:|
    |  TIMESTAMP   | YYYY-MM-DD HH:MM:SS |    Tsoil     |      °C       |
    |    fosse     |   ID (character)    |     zone     | m (zone depth |
    |    depth     |         cm          |   zone_lab   |   character   |
    |      ID      |      sensor ID      |  zmax,zmin   |      cm       |
    |      HV      |  cm3(H2O)/cm3(sol)  |              |               |

-   solarT.csv : used as input in the goplus simulation and to test the adequacy between the 2 sites.

    | **Variable** |      **Units**      |   **Variable**   | **Units** |
    |:------------:|:-------------------:|:----------------:|:---------:|
    |  TIMESTAMP   | YYYY-MM-DD HH:MM:SS |   CMP22_global   |   W.m-2   |
    |  Par_global  |        W.m-2        | CMP22_diffus_Avg |   W.m-2   |
    |  Par_diffus  |        W.m-2        |                  |           |

-   soil_global.csv

    | **Variable** |      **Units**      | **Variable** | **Units** |
    |:------------:|:-------------------:|:------------:|:---------:|
    |  TIMESTAMP   | YYYY-MM-DD HH:MM:SS |      G       |   W.m-2   |
    |     REW      |          %          |    nappe     |     m     |

-   profil.csv: data extracted from the 7 level profil from ICOS. niv04, sousbois, souscouvert, couvert, surcouvert, niv11, niv15 respectively represent the seven height which are depicted in the site figure in the article or in the labelisation document of the site (FR-Bil).

    | **Variable** |      **Units**      | **Variable** | **Units** |
    |:------------:|:-------------------:|:------------:|:---------:|
    |  TIMESTAMP   | YYYY-MM-DD HH:MM:SS |    RH\_\*    |     %     |
    |    T\_\*     |         °C          |    WS\_\*    |     m     |

-   Ts.csv: Surface temperature extracted from the camera. sd columns are the standard deviation for each ROI. Tleaves_next is the columns that is taken as the surface temperature in the graphs.

    | **Variable** |      **Units**      |
    |:------------:|:-------------------:|
    |  TIMESTAMP   | YYYY-MM-DD HH:MM:SS |
    |    T\_\*     |         °C          |
    |  T\_\*\_sd   |         °C          |

-   VT37.csv: used to get the temperature and humidity at 15.6m which is the EC height and to cross calibrate the HMP155 sensors that measure T and RH for the profil (not show in the scripts).

    | **Variable** |      **Units**      | **Variable** | **Units**  |
    |:------------:|:-------------------:|:------------:|:----------:|
    |  TIMESTAMP   | YYYY-MM-DD HH:MM:SS |     hrel     |     %      |
    |    RECORD    |         \-          |    VPD15     |     Pa     |
    |     T_15     |         °C          |    \*\_sd    | same as \* |
    |     Tdew     |         °C          |              |            |

-   goplus.csv

    | **Variable** |      **Units**      |
    |:------------:|:-------------------:|
    |  TIMESTAMP   | YYYY-MM-DD HH:MM:SS |
    | Rn_ecosystem |        W.m-2        |
    |  Rn_canopy   |        W.m-2        |

-   ICOS_dendro_2020.csv: extract from the yearly dendrometric database.

    |                **Variable**                | **Units** |   Variable   |   Units   |
    |:------------------------------------------:|:---------:|:------------:|:---------:|
    |                  TREE_ID                   |    \-     | TREE_status  | character |
    |             TREE_EASTWARD_DIST             |     m     | TREE_comment | character |
    | TREE_NORTHWARD_DISTT\_\*TREE_EASTWARD_DIST |     m     |  TREE_date   | YYYYMMDD  |
    |                  TREE_DBH                  |    cm     |              |           |

-   F_peuplement.csv

    | **Variable** |       **Units**       |
    |:------------:|:---------------------:|
    |  TIMESTAMP   |  YYYY-MM-DD HH:MM:SS  |
    |     Jref     | cm3(H2O)/cm2(sapwood) |
    |   Jref_sd    | cm3(H2O)/cm2(sapwood) |
    |    F_mm_s    |        mm.s-1         |

# packrat/

If it works it should contains all the packages used in this project. use the packrat.lock to generate the same environment that I used to analyse the data and produce the graphs.
