#!/bin/bash
echo GT
python3 scripts/mailalert.py --subject "SA run GT starts"
date
Rscript SA_code_GT.R
echo SGT
python3 scripts/mailalert.py --subject "SA run SGT starts"
date
Rscript SA_code_SGT.R
echo PMT
python3 scripts/mailalert.py --subject "SA run PMT starts"
date
Rscript SA_code_PMT.R
echo end
date
python3 scripts/mailalert.py --subject "SA run finished"
