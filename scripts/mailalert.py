#!/usr/bin/python3

import sys, getopt
import smtplib
import mimetypes
from email.message import EmailMessage
import os 

def mailalert(argv): 
    path = os.getcwd()
    msg = EmailMessage()
    subject = 'no subject given'
    content = "Pray the magic murlock that everything went well."
    to = ["tom.taborski@inrae.fr"]
  
    try:
        opts, args = getopt.getopt(argv,"ht:s:p:",["subject=","text=","to=","attach=","pwd="])
    except getopt.GetoptError:
        print ('test.py --subject <string> --text = <string> --to = <mailadress>')
        sys.exit(2)
    
    for opt, arg in opts:
        if opt == '-h': # help printed
            print ('test.py --subject <string> --text = <string>')
            sys.exit()
        elif opt in ("-s","--subject"): # kinda explicit
            subject = arg
        elif opt in ("-t","--text"): # body 
            content = arg
        elif opt in ("--to"): # kinda explicit
            to = arg
        elif opt in ("--attach"): # file to attach with the path
            filename = arg
        elif opt in ("-p","pwd"): # optionnal, should get the pwd in a file but you can provide it there.
            gmail_password = arg
    
    gmail_user = 'generalealeeeerte@gmail.com'
    
    if "gmail_password" not in locals():
        if "most"  in os.getcwd():
            m = open("../mdp_dumb","r")
        else:
            m = open("/home/ttaborski/Documents/INRA/mdp_dumb")
        gmail_password = m.readline()
        m.close()


    
    msg['Subject'] = subject
    msg.set_content(content)
    msg['From'] = gmail_user
    msg['To'] = to
    
    with open(filename, 'rb') as fp:
        file_data = fp.read()
        maintype, _, subtype = (mimetypes.guess_type(filename)[0] or 'application/octet-stream').partition("/")
        msg.add_attachment(file_data, maintype=maintype, subtype=subtype, filename=filename)

    try:
        server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        server.ehlo()
        server.login(gmail_user, gmail_password)
        server.send_message(msg)
        server.close()

        print("mail sent with:")
        print("Subject: ",msg['Subject'])
        print("text sent: ", content)
    except Exception as e:
        print(e)
        print ('Something went wrong...')
        


if __name__ == "__main__":
   mailalert(sys.argv[1:]) # 1 to not take the file name in account. 
